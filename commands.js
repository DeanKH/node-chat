var admins = {}; // should probably be created in app.js

var commands = {
	// Allows admins to login
	login: function (data, io, socket) {

		if(data == 'pass') {
			admins[socket.username] = true;
			io.sockets.emit('sendnotification', socket.username + ' logged in as admin', 'blue');
		} else {
			clients[socket.id].emit('sendnotification', 'Login failed!', 'red');
		}
		
		io.sockets.emit('updateusers', usernames, admins);

	},
	// Allows admins to logout
	logout: function (data, io, socket) {

		if(!isAdmin(socket.username, true)) {
			delete admins[socket.username];
			io.sockets.emit('sendnotification', socket.username + ' logged out of admin', 'blue');
			io.sockets.emit('updateusers', usernames, admins);
		}

	},
	// Displays a list of currently logged-in admins
	admins: function (data, io, socket) {

		clients[users[socket.username]].emit('sendnotification', '<br />Admins:', 'grey');
		for(user in admins) {
			clients[users[socket.username]].emit('sendnotification', user, 'grey');
		}
		clients[users[socket.username]].emit('sendnotification', '================', 'grey');

	},
	// Redirects a specific user to a specified URL
	redirect: function (data, io, socket) {

		// Nasty as hell
		var user = (data.substr(0,data.indexOf(' ')) == "") ? data : data.substr(0,data.indexOf(' '))
		,	reason = (data.substr(0,data.indexOf(' ')) == "") ? null : data.substr(data.indexOf(' ')+1);

		// Check if the user issuing the command logged in as an admin
		if(!isAdmin(socket, true)) return;

		// Check that the user who should be kicked still exists
		if(!doesUserExist(user, socket)) return;

		var string = "<script>window.location='"+reason+"'</script>";

		clients[users[user]].emit('sendnotification', string, 'blue');
		io.sockets.emit('sendnotification', user + ' was redirected by ' + socket.username + " to " + reason, 'grey');

	},
	// Disconnects the specified user
	kick: function (data, io, socket) {
		
		// Nasty as hell
		var user = (data.substr(0,data.indexOf(' ')) == "") ? data : data.substr(0,data.indexOf(' '))
		,	reason = (data.substr(0,data.indexOf(' ')) == "") ? null : data.substr(data.indexOf(' ')+1);

		// Check if the user issuing the command logged in as an admin
		if(!isAdmin(socket, true)) return;

		// Check that the user who should be kicked still exists
		if(!doesUserExist(user, socket)) return;

		// Kick the user
		var reason = (reason != null) ? " [reason: " + reason + "]" : "";
		clients[users[user]].emit('sendnotification', 'You were kicked by ' + socket.username + reason, 'red');
		clients[users[user]].disconnect();
		io.sockets.emit('sendnotification', user + ' was kicked by ' + socket.username + reason, 'grey');

	},
	// Shows list of currently muted users
	mutelist: function (data, io, socket) {
		
		// Check if the user issuing the command logged in as an admin
		if(!isAdmin(socket, true)) return;

		clients[socket.id].emit('sendnotification', '<br />Muted users:', 'grey');

		if(Object.keys(muted).length < 1) {
			clients[socket.id].emit('sendnotification', 'No one is muted', 'grey');
		} else {
			for(var user in muted) {
			clients[socket.id].emit('sendnotification', user, 'grey');
			}
		}
		clients[socket.id].emit('sendnotification', '================', 'grey');

	},
	// Mutes the specified user
	mute: function (data, io, socket) {

		// Nasty as hell
		var user = (data.substr(0,data.indexOf(' ')) == "") ? data : data.substr(0,data.indexOf(' '))
		,	reason = (data.substr(0,data.indexOf(' ')) == "") ? null : data.substr(data.indexOf(' ')+1);

		// Check if the user issuing the command logged in as an admin
		if(!isAdmin(socket, true)) return;

		// Check that the user who should be kicked still exists
		if(!doesUserExist(user, socket)) return;

		// Mute the user
		var reason = (reason != null) ? " [reason: " + reason + "]" : "";
		muted[user] = true;
		clients[users[user]].emit('sendnotification', 'You were muted by ' + socket.username + reason, 'red');
		io.sockets.emit('sendnotification', user + ' was muted by ' + socket.username + reason, 'grey');

	},
	// Unmutes the specified user
	unmute: function (data, io, socket) {

		// Nasty as hell
		var user = (data.substr(0,data.indexOf(' ')) == "") ? data : data.substr(0,data.indexOf(' '))
		,	reason = (data.substr(0,data.indexOf(' ')) == "") ? null : data.substr(data.indexOf(' ')+1);

		// Check if the user issuing the command logged in as an admin
		if(!isAdmin(socket, true)) return;

		// Check that the user who should be kicked still exists
		if(!doesUserExist(user, socket)) return;

		// Mute the user
		var reason = (reason != null) ? " [reason: " + reason + "]" : "";
		delete muted[user];
		clients[users[user]].emit('sendnotification', 'You were unmuted by ' + socket.username + reason, 'red');
		io.sockets.emit('sendnotification', user + ' was unmuted by ' + socket.username + reason, 'grey');

	},
	// Bans the specified user via IP
	ban: function (data, io, socket) {
		
		// Nasty as hell
		var user = (data.substr(0,data.indexOf(' ')) == "") ? data : data.substr(0,data.indexOf(' '))
		,	reason = (data.substr(0,data.indexOf(' ')) == "") ? null : data.substr(data.indexOf(' ')+1);

		// Check if the user issuing the command logged in as an admin
		if(!isAdmin(socket, true)) return;

		// Check that the user who should be kicked still exists
		if(!doesUserExist(user, socket)) return;

		IPbans[IPs[user]] = true;

	}, 
	// Shows the help menu
	help: function (data, io, socket) {

		clients[socket.id].emit('sendnotification', 'Commands:', 'grey');

		// Show admin commands if the user is logged in
		if(isAdmin(socket, false)) {
			clients[socket.id].emit('sendnotification', '#logout - logout of admin', 'grey');
			clients[socket.id].emit('sendnotification', '#kick [user] [reason:optional] - kick user', 'grey');
			clients[socket.id].emit('sendnotification', '#mute [user] [reason:optional] - mutes user', 'grey');
			clients[socket.id].emit('sendnotification', '#unmute [user] [reason:optional] - unmutes user', 'grey');
			clients[socket.id].emit('sendnotification', '#mutelist - list muted users', 'grey');
			clients[socket.id].emit('sendnotification', '#redirect [user] [url] - redirect user', 'grey');

		// otherwise show normal user commands
		} else {
			clients[socket.id].emit('sendnotification', '#login [password] - login as admin', 'grey');
		}
		
		// Show to all
		clients[socket.id].emit('sendnotification', '#help - show this menu', 'grey');

	},
	// generate random text 
	lorem: function (data, io, socket) {
		clients[socket.id].emit('updatechat', 'Some Guy', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod');
		clients[socket.id].emit('updatechat', 'Some Guy', 'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse');
		clients[socket.id].emit('updatechat', 'Some Guy', 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo');
		clients[socket.id].emit('updatechat', 'Some Guy', 'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
		clients[socket.id].emit('updatechat', 'Some Guy', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod');
		clients[socket.id].emit('updatechat', 'Some Guy', 'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse');
		clients[socket.id].emit('updatechat', 'Some Guy', 'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non');
		clients[socket.id].emit('updatechat', 'Some Guy', 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,');
		clients[socket.id].emit('updatechat', 'Some Guy', 'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
		clients[socket.id].emit('updatechat', 'Some Guy', 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,');
		clients[socket.id].emit('updatechat', 'Some Guy', 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo');
		clients[socket.id].emit('updatechat', 'Some Guy', 'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
		clients[socket.id].emit('updatechat', 'Some Guy', 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo');
		clients[socket.id].emit('updatechat', 'Some Guy', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod');
		clients[socket.id].emit('updatechat', 'Some Guy', 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,');
		clients[socket.id].emit('updatechat', 'Some Guy', 'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non');
		clients[socket.id].emit('updatechat', 'Some Guy', 'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse');
		clients[socket.id].emit('updatechat', 'Some Guy', 'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non');
		clients[socket.id].emit('updatechat', 'Some Guy', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod');
		clients[socket.id].emit('updatechat', 'Some Guy', 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo');
		clients[socket.id].emit('updatechat', 'Some Guy', 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,');
		clients[socket.id].emit('updatechat', 'Some Guy', 'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse');
		clients[socket.id].emit('updatechat', 'Some Guy', 'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non');
		clients[socket.id].emit('updatechat', 'Some Guy', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod');
		clients[socket.id].emit('updatechat', 'Some Guy', 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,');
		clients[socket.id].emit('updatechat', 'Some Guy', 'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse');
		clients[socket.id].emit('updatechat', 'Some Guy', 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo');
		clients[socket.id].emit('updatechat', 'Some Guy', 'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
		clients[socket.id].emit('updatechat', 'Some Guy', 'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
		clients[socket.id].emit('updatechat', 'Some Guy', 'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non');
		clients[socket.id].emit('updatechat', 'Some Guy', 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo');
		clients[socket.id].emit('updatechat', 'Some Guy', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod');
		clients[socket.id].emit('updatechat', 'Some Guy', 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,');
		clients[socket.id].emit('updatechat', 'Some Guy', 'consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse');
		clients[socket.id].emit('updatechat', 'Some Guy', 'cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non');
		clients[socket.id].emit('updatechat', 'Some Guy', 'proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
	},
	me: function(data, io,socket) {

		var text = data;
		io.sockets.emit('sendnotification', "* " + socket.username + " " + text, 'blue');
	}

};
// All commands run through this function. It's called within app.js.
function execute (cmd, data, io, socket) {

	if (typeof commands[cmd] !== 'function')
		clients[socket.id].emit('sendnotification', cmd + ' command not recognised', 'red');
	else
		commands[cmd](data, io, socket);
}
// Check if the user on the socket passed is an admin
function isAdmin (socket, informUser) {
	if(typeof(admins[socket.username]) == 'undefined') {
		if (informUser)
			clients[socket.id].emit('sendnotification', 'You are not logged in as an admin', 'red');
		return false;
	}
	return true;
}
// Checks if a user is still connected
function doesUserExist (username, socket) {
	// Check that the user who should be kicked still exists
	console.log('username: ' + username + '.');
	console.log(JSON.stringify(users))
	if(typeof(users[username]) == 'undefined') {
		clients[socket.id].emit('sendnotification', 'User not found', 'grey');
		return false;
	}
	return true;
}
// Get's the connected sockets IP address
function getClientIp(req) {
  var ipAddress;
  // Amazon EC2 / Heroku workaround to get real client IP
  var forwardedIpsStr = req.header('x-forwarded-for'); 
  if (forwardedIpsStr) {
    // 'x-forwarded-for' header may return multiple IP addresses in
    // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
    // the first one
    var forwardedIps = forwardedIpsStr.split(',');
    ipAddress = forwardedIps[0];
  }
  if (!ipAddress) {
    // Ensure getting client IP address still works in
    // development environment
    ipAddress = req.connection.remoteAddress;
  }
  return ipAddress;
};

exports.execute = execute;
exports.admins = admins;
	usernames = {}
,	typers = {}
,	clients = {}
,	users = {}
,	muted = {}
,	IPs = {}
,	IPbans = {}
,	rooms = {}
,	homeRoomID = '';

var express = require('express')
,	app = express()
,	http = require('http')
,	server = http.createServer(app)
,	io = require('socket.io').listen(server)
,	commands = require('./commands.js')
,	sanitize = require('validator').sanitize
,   config = require('./config.js')

// mongodb connection
// ,   dbURL = config.dbUser+":"+config.dbPass+"@"+config.dbHost+"/"+config.dbName
,	dbURL = "localhost/chatApp"
,	dbCollections = ["rooms", "users"]
,	db = require("mongojs").connect(dbURL, dbCollections);

// Populate the rooms array with the active rooms in the db
db.rooms.find({active: true}, function(err, dbRooms) {

	if ( err || !dbRooms ) console.log("No dbRooms found");
	else dbRooms.forEach( function (room) {
		// console.log(room);
		if(room.name == "Home Room") homeRoomID = room._id;
		rooms[room._id] = room;
	});
	console.log(rooms);

});



server.listen(1337);
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
	res.sendfile(__dirname + '/index.html');
});

io.sockets.on('connection', function(socket) {

	if(socket.username == "" && socket.username == null) return false; // WUT???!

	// check username exists already
	if(typeof(usernames[socket.username]) != 'undefined') 
		socket.username = socket.username + " (1)";

	var hs = socket.handshake;

	socket.curRoom = homeRoomID;
	socket.join(socket.curRoom); // join the home room

	// update available rooms
	socket.emit('updateRooms', rooms, homeRoomID);

	// send connection message + initial motd
	socket.emit('sendnotification', 'Connecting to ' + rooms[homeRoomID].name + '...', 'red');
	socket.emit('sendnotification', 'You have connected', 'red');
	socket.emit('sendnotification', rooms[homeRoomID].motd + "<br />", 'grey');

	console.log(JSON.stringify(IPs));

	clients[socket.id] = socket;

	socket.on('adduser', function(username) {
		if (typeof(username) !== 'undefined') {
			IPs[username] = socket.handshake.headers['x-forwarded-for'] || socket.handshake.address.address;

			db.users.save({name: username, currentRoom: socket.curRoom}, function(err, saved) {
				if( err || !saved ) console.log("User not saved");
 				else console.log("User saved");
 				// console.log(saved);
 				socket.dbID = saved._id.toString();
				socket.username = username;
				usernames[username] = socket.dbID;
				console.log("USERNAME ID: " + socket.dbID);
				users[socket.dbID] = socket.id;
				socket.broadcast.to(socket.curRoom).emit('sendnotification', username + ' has connected', 'green');
				io.sockets.emit('updateusers', usernames, commands.admins);
			});

		}
	});
	
	socket.on('switchRoom', function(newRoom) {

		console.log('Current: ' + socket.curRoom + " | New: " + newRoom);
		// leave the current room (stored in session)
		socket.leave(socket.curRoom);
		// join new room, received as function parameter
		socket.join(newRoom);
		socket.emit('updatechat', 'SERVER', 'You have connected to '+ rooms[newRoom].name);
		// send new MOTD to user
		socket.emit('sendnotification', rooms[newRoom].motd, 'red');
		// sent message to OLD room
		socket.broadcast.to(socket.curRoom).emit('sendnotification', socket.username+' has left this room', 'orange');
		// update socket session room title
		socket.curRoom = newRoom;
		socket.broadcast.to(newRoom).emit('sendnotification', socket.username+' has connected', 'green');
		socket.emit('updateRooms', rooms, newRoom);
	});

	socket.on('sendchat', function(data) {
		if(data != "") {
			data = sanitize(data).xss();
			socket.username = sanitize(socket.username).xss();
			console.log(socket.username + " : " + socket.curRoom);

			// check if the user is muted, if so, we don't want to emit their messages
			if(typeof(muted[socket.username]) != 'undefined') {
				clients[users[socket.username]].emit('sendnotification', 'You are muted', 'red');
				return;
			}

			io.sockets.in(socket.curRoom).emit('updatechat', socket.username, data);
		}
	});

	socket.on('disconnect', function() {
		if (typeof(socket.username) !== 'undefined') {

			delete usernames[socket.username];
			delete commands.admins[socket.username];
			delete typers[socket.username];

			delete clients[socket.id];
			delete users[socket.username];

			console.log("DB ID: " + socket.dbID);
			var foo = db.ObjectId;
			db.users.remove({_id: foo(socket.dbID)});

			socket.broadcast.to(socket.curRoom).emit('updateTypers', typers);
			socket.broadcast.to(socket.curRoom).emit('updateusers', usernames, commands.admins);
			socket.broadcast.to(socket.curRoom).emit('sendnotification', socket.username + ' has disconnected', 'orange');
		}
	});

	socket.on('sendcommand', function(command) {

		if(command.indexOf(' ') > 0) {
			var cmd = command.substr(0, command.indexOf(' '));
		} else {
			var cmd = command;
		}

		cmd = cmd.substring(1, cmd.length);

		var data = command.substr(command.indexOf(' ')+1); // strip the command

		commands.execute(cmd, data, io, socket);

	});

	socket.on('isTyping', function(typing) {

		if(typing == true)
			typers[socket.dbID] = true;
		else {
			delete typers[socket.dbID];
		}
		
		io.sockets.emit('updateTypers', typers);
	});

	socket.on('whisper', function(clientId, message) {

		clients[socket.id].emit('sendnotification', socket.username + ' whispers: ' + message, 'grey');
		clients[users[clientId]].emit('sendnotification', socket.username + ' whispers: ' + message, 'grey');

	});

	socket.on('updateRooms', function() {
		rooms = {};
		db.rooms.find({active: true}, function(err, dbRooms) {

			if ( err || !dbRooms ) console.log("No dbRooms found");
			else dbRooms.forEach( function (room) {
				rooms[room._id] = room;
			});
			// send client the updated rooms
			socket.emit('updateRooms', rooms, socket.curRoom);

		});
	});

});

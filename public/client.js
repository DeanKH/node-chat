var username = ''
,	isTyping = false
,	usersTyping = {}
,	socket = io
,	gotUsername = false
,	missedMessages = 0
,	origDocTitle = ''
,	showTypingText = sessionStorage['showTypingText']; 

$(function() {

	origDocTitle = document.title;

	// gotUsername = (localStorage["username"] != "" && localStorage["username"] != null) ? true : false;
	gotUsername = (sessionStorage["username"] != undefined && sessionStorage["username"] != null) ? true : false;
	console.log("uname: " + sessionStorage["username"]);
	// alert(gotUsername);
	// alert(localStorage["username"]);

	// Show username input if not set
	if(!gotUsername) {
		$('.modal-overlay').fadeIn('fast');
		$('#username').focus();
		$('#setusername').on('click', connect);
	}
	else connect();

	// Listen for pressing enter on the username input
	$('#username').on('keyup', function(event) {

		if(event.which == 13) connect();

	})

	$('#message').on('keyup', function(event) {

		var $this = $(this);
		var wasTyping = isTyping;

		isTyping = ($this.val() == "") ? false : true;

		if(event.which == 13) {
			var message = $('#message').val();
			$('#message').val();

			if(message.substring(0,1) == "#")
				socket.emit('sendcommand', message);
			else
				socket.emit('sendchat', message);

			$this.val('');
			isTyping = false;
		}

		if((wasTyping != true && isTyping == true) || isTyping == false) {
			socket.emit('isTyping', isTyping);
		}
	});

	// Reset title when the window has gained focus
	// 		- Bug: doesn't work on chrome tab titles
	$(window).focus(function() {
		if(document.title !== origDocTitle) {
			document.title = origDocTitle;
			missedMessages = 0;
		}
	});

	// Display the whisper window
	$('#users').on('click', '.whisper', function () {
		$('#whisper-to').val($(this).data('socket-id'));
		$('.whisper-overlay').fadeIn('fast');
		$('#whisper-text').focus();
	});

	// Listen for pressing enter on the username input
	$('#whisper-text').on('keyup', function(event) {

		if(event.which == 13) {
			socket.emit('whisper', $('#whisper-to').val(), $('#whisper-text').val());
			$('#whisper-text').val('');
			$('#whisper-to').val('');
			$('.whisper-overlay').fadeOut('fast');
		}

	});

	// Sending a whisper
	$('#send-whisper').on('click', function () {
		socket.emit('whisper', $('#whisper-to').val(), $('#whisper-text').val());
		$('#whisper-text').val('');
		$('#whisper-to').val('');
		$('.whisper-overlay').fadeOut('fast');
	});

	$('.show-servers-button').on('click', function () {

		socket.emit('updateRooms');
		$('.servers-overlay').fadeIn('fast');
	});

	$('.close-servers-overlay').on('click', function () {
		$('.servers-overlay').fadeOut('fast');
	});

	$('#server-list').on('click', '.join-server', function () {
		$('.servers-overlay').fadeOut('fast');
		socket.emit('switchRoom', $(this).data('server-id'));
		$('#conversation').empty();
	});

	// Listen for the escape key on the overlays
	$(document).on('keyup', function (event) {
		if(event.which == 27) {
			$('.modal').fadeOut('fast');
		}
	});

	// Need to get working

	// $('.overlay').on('click', function (e) {
	// 	if(!$(e.target).is($('.overlay').children())) {
	// 		e.preventDefault();
	// 		$('.modal').fadeOut('fast');
	// 	}
	// });

	$('.refresh-servers').on('click', function () {
		socket.emit('updateRooms');
	})

});

function connect() {

	if(!gotUsername) {
		username = $('#username').val();
		// localStorage["username"] = username;
		sessionStorage["username"] = username;
	} else {
		// username = localStorage["username"];
		username = sessionStorage["username"];
	}

	if(username == "") {
		alert('you need to enter a username');
		return false;
	}

	$('.modal-overlay').fadeOut();
	$('#message').focus();

	socket = io.connect('http://' + window.location.hostname + ':1337');

	socket.on('connect', function() {
		socket.emit('adduser', username);
	});

	socket.on('updateTypers', function(typers) {

		var count = 0;
		console.log(JSON.stringify(typers));
		usersTyping = {};
		$.each(typers, function(key, value) {
				usersTyping[key] = true;
		});

		$('.user-item').each(function() {
			var curUsername = $(this).data('username');
			console.log(curUsername + " - " + typeof(usersTyping[curUsername]));
			if(!(curUsername in usersTyping)) {

				$('#keyboard-' + curUsername).hide('fast', function () {
					$('#keyboard-' + curUsername).remove();
				});

			} else {
				$('#user-' + curUsername).append('<span id="keyboard-'+curUsername+'" style="margin-left:5px;display:none"><img src="images/keyboard.png" /></span>');
				$('#keyboard-' + curUsername).show('fast');
			}
		});

	});

	socket.on('updatechat', function(username, data) {

		if( !document.hasFocus() ) {
			missedMessages++;
			document.title = "[" + missedMessages + "] New Messages";
		}
		
		// show emotes - shoddy as fuck
		data = data.replace(':)', "<span class='smile'></span>");
		data = data.replace(':s', "<span class='worried'></span>");
		data = data.replace('<3', "<span class='in-love'></span>");
		data = data.replace('shit', "<span class='shit'></span>");

		$el = $('#conversation');
		$el.append('<b>' + username + '</b>: ' + data + '<br />');
		$('#convo-area').animate({ scrollTop: $el.prop("scrollHeight")}, {duration: 1000, queue:false});

	});

	socket.on('sendnotification', function(data, color) {

		$el = $('#conversation');
		$('#conversation').append('<span class="'+color+'">' + data + '</span><br />');
		$('#convo-area').animate({ scrollTop: $el.prop("scrollHeight")}, {duration: 1000, queue:false});

	});

	socket.on('updateusers', function(data, admins) {

		$('#users').empty();
		$.each(data, function(key, value) {
			if(admins[key] == true)	
				$('#users').append('<hr /><div class="orange"><span class="user-item" id="user-'+value+'" data-username="'+value+'">' + key + '</span> <a class="whisper" data-socket-id="'+value+'">whisper</a></div>');
			else
				$('#users').append('<hr /><div><span class="user-item" id="user-'+value+'" data-username="'+value+'">' + key + '</span> <a class="whisper" data-socket-id="'+value+'">whisper</a></div>');
		});

	});

	socket.on('updateRooms', function (rooms, curRoom) {

		$('#server-list').empty();
		console.log('length: ' + Object.keys(rooms).length);
		if(Object.keys(rooms).length > 0) {
			$.each(rooms, function (key, details) {

				if(key == curRoom) {
					$('.chrome-tab-current').find('.chrome-tab-title').text(details.name);
				}

				var join = (key != curRoom) ? "<div class='join-server' data-server-id='"+key+"'>Join</div>" : "<div class='current-server'>Current Room</div>";
				$('#server-list').append('<div class="server"><div class="server-name">' + 
										 details.name + 
										 "</div><div class='server-desc'>" + 
										 details.description + 
										 "</div>" +
										 join +
										 "<div class='clear'></div>" + 
										 "</div>");
			});
		} else {
			$('#server-list').append('<p>No rooms found</p>');
		}

	});

	// Update the current rooms
	socket.emit('updateRooms');
}

function escapeRegExp(str) {
	return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}